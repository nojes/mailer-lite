﻿namespace MailerLite
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textTo = new System.Windows.Forms.TextBox();
			this.groupData = new System.Windows.Forms.GroupBox();
			this.listRecipients = new System.Windows.Forms.ListBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textSubject = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnSend = new System.Windows.Forms.Button();
			this.btnAttachFiles = new System.Windows.Forms.Button();
			this.groupBody = new System.Windows.Forms.GroupBox();
			this.rtextMessage = new System.Windows.Forms.RichTextBox();
			this.listFiles = new System.Windows.Forms.ListBox();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.labelHintFiles = new System.Windows.Forms.Label();
			this.btnAddRecipient = new System.Windows.Forms.Button();
			this.groupData.SuspendLayout();
			this.groupBody.SuspendLayout();
			this.SuspendLayout();
			// 
			// textTo
			// 
			this.textTo.Location = new System.Drawing.Point(49, 17);
			this.textTo.Name = "textTo";
			this.textTo.Size = new System.Drawing.Size(284, 20);
			this.textTo.TabIndex = 1;
			this.textTo.TextChanged += new System.EventHandler(this.textTo_TextChanged);
			this.textTo.Enter += new System.EventHandler(this.textTo_Enter);
			this.textTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textTo_KeyDown);
			// 
			// groupData
			// 
			this.groupData.Controls.Add(this.btnAddRecipient);
			this.groupData.Controls.Add(this.listRecipients);
			this.groupData.Controls.Add(this.label2);
			this.groupData.Controls.Add(this.textSubject);
			this.groupData.Controls.Add(this.label1);
			this.groupData.Controls.Add(this.textTo);
			this.groupData.Location = new System.Drawing.Point(12, 12);
			this.groupData.Name = "groupData";
			this.groupData.Size = new System.Drawing.Size(660, 107);
			this.groupData.TabIndex = 2;
			this.groupData.TabStop = false;
			// 
			// listRecipients
			// 
			this.listRecipients.FormattingEnabled = true;
			this.listRecipients.Location = new System.Drawing.Point(352, 17);
			this.listRecipients.Name = "listRecipients";
			this.listRecipients.Size = new System.Drawing.Size(302, 56);
			this.listRecipients.TabIndex = 6;
			this.listRecipients.Visible = false;
			this.listRecipients.DoubleClick += new System.EventHandler(this.listRecipients_DoubleClick);
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 84);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(37, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Тема:";
			// 
			// textSubject
			// 
			this.textSubject.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.textSubject.Location = new System.Drawing.Point(49, 81);
			this.textSubject.Name = "textSubject";
			this.textSubject.Size = new System.Drawing.Size(605, 20);
			this.textSubject.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(36, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Кому:";
			// 
			// btnSend
			// 
			this.btnSend.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.btnSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnSend.Location = new System.Drawing.Point(19, 488);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(121, 32);
			this.btnSend.TabIndex = 0;
			this.btnSend.Text = "Отправить";
			this.btnSend.UseVisualStyleBackColor = true;
			this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
			// 
			// btnAttachFiles
			// 
			this.btnAttachFiles.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.btnAttachFiles.Location = new System.Drawing.Point(165, 489);
			this.btnAttachFiles.Name = "btnAttachFiles";
			this.btnAttachFiles.Size = new System.Drawing.Size(108, 31);
			this.btnAttachFiles.TabIndex = 4;
			this.btnAttachFiles.Text = "Прикрепить файл";
			this.btnAttachFiles.UseVisualStyleBackColor = true;
			this.btnAttachFiles.Click += new System.EventHandler(this.btnAttachFiles_Click);
			// 
			// groupBody
			// 
			this.groupBody.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.groupBody.Controls.Add(this.rtextMessage);
			this.groupBody.Location = new System.Drawing.Point(12, 125);
			this.groupBody.Name = "groupBody";
			this.groupBody.Size = new System.Drawing.Size(660, 271);
			this.groupBody.TabIndex = 3;
			this.groupBody.TabStop = false;
			// 
			// rtextMessage
			// 
			this.rtextMessage.Location = new System.Drawing.Point(7, 19);
			this.rtextMessage.Name = "rtextMessage";
			this.rtextMessage.Size = new System.Drawing.Size(647, 245);
			this.rtextMessage.TabIndex = 0;
			this.rtextMessage.Text = "";
			// 
			// listFiles
			// 
			this.listFiles.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.listFiles.FormattingEnabled = true;
			this.listFiles.Location = new System.Drawing.Point(19, 395);
			this.listFiles.Name = "listFiles";
			this.listFiles.Size = new System.Drawing.Size(647, 82);
			this.listFiles.TabIndex = 1;
			this.listFiles.Visible = false;
			this.listFiles.SelectedIndexChanged += new System.EventHandler(this.listFiles_SelectedIndexChanged);
			this.listFiles.DoubleClick += new System.EventHandler(this.listFiles_DoubleClick);
			// 
			// openFileDialog
			// 
			this.openFileDialog.FileName = "openFileDialog";
			// 
			// labelHintFiles
			// 
			this.labelHintFiles.AutoSize = true;
			this.labelHintFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelHintFiles.Location = new System.Drawing.Point(480, 480);
			this.labelHintFiles.Name = "labelHintFiles";
			this.labelHintFiles.Size = new System.Drawing.Size(186, 12);
			this.labelHintFiles.TabIndex = 5;
			this.labelHintFiles.Text = "*Кликните дважды чтобы открепить файл";
			// 
			// btnAddRecipient
			// 
			this.btnAddRecipient.Location = new System.Drawing.Point(258, 43);
			this.btnAddRecipient.Name = "btnAddRecipient";
			this.btnAddRecipient.Size = new System.Drawing.Size(75, 23);
			this.btnAddRecipient.TabIndex = 7;
			this.btnAddRecipient.Text = "Добавить";
			this.btnAddRecipient.UseVisualStyleBackColor = true;
			this.btnAddRecipient.Click += new System.EventHandler(this.btnAddRecipient_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(684, 532);
			this.Controls.Add(this.labelHintFiles);
			this.Controls.Add(this.listFiles);
			this.Controls.Add(this.btnAttachFiles);
			this.Controls.Add(this.groupBody);
			this.Controls.Add(this.btnSend);
			this.Controls.Add(this.groupData);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Новое письмо";
			this.groupData.ResumeLayout(false);
			this.groupData.PerformLayout();
			this.groupBody.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.TextBox textTo;
		private System.Windows.Forms.GroupBox groupData;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textSubject;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnSend;
		private System.Windows.Forms.Button btnAttachFiles;
		private System.Windows.Forms.GroupBox groupBody;
		private System.Windows.Forms.RichTextBox rtextMessage;
		private System.Windows.Forms.ListBox listFiles;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.Label labelHintFiles;
		private System.Windows.Forms.ListBox listRecipients;
		private System.Windows.Forms.Button btnAddRecipient;
	}
}

