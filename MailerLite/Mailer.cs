﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MailerLite
{
	public class Mailer
	{
		public string SmtpServer { get; set; }
		public string SmtpRequiresAuthentication { get; set; }
		public string SmtpUseSsl { get; set; }
		public int SmtpPort { get; set; }
		public string SmtpUser { get; set; }
		public string SmtpPassword { get; set; }
		public int SmtpTimeoutInMilliseconds { get; set; }
		public string SmtpPreferredEncoding { get; set; }
		public MailerMessage Message { get; set; }

		private SmtpClient client;
		private MailMessage mail;

		public Mailer()
		{
			NameValueCollection appSettings = ConfigurationManager.AppSettings;
			SmtpServer = appSettings["SMTPServer"];
			SmtpRequiresAuthentication = appSettings["SMTPRequiresAuthentication"];
			SmtpUseSsl = appSettings["SMTPUseSsl"];
			SmtpPort = Convert.ToInt32(appSettings["SMTPPort"]);
			SmtpUser = appSettings["SMTPUser"];
			SmtpPassword = appSettings["SMTPPassword"];
		}

		private void Init()
		{
			client = new SmtpClient
			{
				Host = SmtpServer,
				Port = SmtpPort,
				EnableSsl = Convert.ToBoolean(SmtpUseSsl),
				Credentials = new NetworkCredential(SmtpUser, SmtpPassword),
				DeliveryMethod = SmtpDeliveryMethod.Network
			};

			mail = new MailMessage
			{
				From = new MailAddress(SmtpUser),
				Subject = Message.Caption,
				Body = Message.Message
			};

			foreach (string recipient in Message.Recipients) {
				mail.To.Add(new MailAddress(recipient));
			}

			if (Message.AttachFiles.Count != 0) {
				foreach (string attachFile in Message.AttachFiles) {
					mail.Attachments.Add(new Attachment(attachFile));
				}
			}
		}


		public bool SendMail()
		{
			try {
				Init();

				client.Send(mail);
				mail.Dispose();
			}
			catch (Exception ex) {
				throw new Exception(ex.Message);
			}

			return true;
		}

		public async Task<bool> SendMailAsync()
		{
			try {
				Init();

				await client.SendMailAsync(mail);
				mail.Dispose();
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

			return true;
		}
	}
}
