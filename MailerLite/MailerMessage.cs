﻿using System;
using System.Collections.Generic;

namespace MailerLite
{
	public class MailerMessage
	{
		public List<string> Recipients { get; set; }
		public string Caption { get; set; }
		public string Message { get; set; }
		public List<string> AttachFiles { get; set; }

		public MailerMessage()
		{
			Recipients = new List<string>();
			AttachFiles = new List<string>();
		}
	}
}
