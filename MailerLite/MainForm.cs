﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailerLite.Properties;

namespace MailerLite
{
	public partial class MainForm : Form
	{
		private const int MAX_HEIGHT = 490;
		private const int MARGIN_MD = 20;
		private bool isAttached;

		private Mailer mailer;

		public MainForm()
		{
			InitializeComponent();

			mailer = new Mailer();
			Height = MAX_HEIGHT;
			btnAttachFiles.Click += listFiles_SelectedIndexChanged;
			textTo.KeyDown += (sender, args) => ToogleRecipients();
			listRecipients.DoubleClick += (sender, args) => ToogleRecipients();
		}

		private void ToogleRecipients()
		{
			listRecipients.Visible = listRecipients.Items.Count != 0;
		}

		private void btnAttachFiles_Click(object sender, EventArgs e) 
		{
			if (!isAttached) {
				ShowAttachments();
			}

			if (openFileDialog.ShowDialog() == DialogResult.OK) {
				listFiles.Items.Add(Convert.ToString(openFileDialog.FileName));
			}
		}

		private void ShowAttachments()
		{
			isAttached = true;

			int new_height = listFiles.Height + MARGIN_MD;
			Height += new_height;
			groupBody.Height += new_height;
			listFiles.Visible = true;
		}

		private void HideAttachments()
		{
			isAttached = false;

			int new_height = listFiles.Height + MARGIN_MD;
			Height -= new_height;
			groupBody.Height -= new_height;
			listFiles.Visible = false;
		}


		private async void btnSend_Click(object sender, EventArgs e)
		{
			if (listRecipients.Items.Count != 0) {
				string btnSendText = btnSend.Text;
				btnSend.Text = "Отправка...";
				btnSend.Enabled = false;

				if (await SendMailAsync()) {
					btnSend.Text = btnSendText;
					btnSend.Enabled = true;

					string msg = "";
					foreach (var item in mailer.Message.Recipients) {
						msg += $"Письмо к {item} успешно отправлено! \n\r";
					}
					MessageBox.Show(msg, "Письмо отправлено", 
						MessageBoxButtons.OK, MessageBoxIcon.Information
					);
				}
				else {
					MessageBox.Show("Попробуйте еще раз.", Resources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			else {
				MessageBox.Show("Укажите хотя бы одного получателя.", Resources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private async Task<bool> SendMailAsync()
		{
			MailerMessage message = new MailerMessage() {
				Caption = textSubject.Text,
				Message = rtextMessage.Text,
			};

			foreach (var item in listRecipients.Items) {
				message.Recipients.Add(item.ToString());
			}

			if (listFiles.Items.Count != 0) {
				foreach (var item in listFiles.Items) {
					message.AttachFiles.Add(item.ToString());
				}
			}

			mailer.Message = message;

			return await mailer.SendMailAsync();
		}

		private void listFiles_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (listFiles.Items.Count == 0) {
				HideAttachments();
			}
		}

		private void listFiles_DoubleClick(object sender, EventArgs e)
		{
			listFiles.Items.RemoveAt(listFiles.SelectedIndex);
		}

		private void textTo_Enter(object sender, EventArgs e) {}
		private void textTo_TextChanged(object sender, EventArgs e) {}

		private void textTo_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				AddRecipient();
			}
		}

		private void listRecipients_DoubleClick(object sender, EventArgs e)
		{
			listRecipients.Items.RemoveAt(listRecipients.SelectedIndex);
		}

		private void btnAddRecipient_Click(object sender, EventArgs e)
		{
			AddRecipient();
		}

		private void AddRecipient()
		{
			if (!string.IsNullOrEmpty(textTo.Text)) {
				listRecipients.Items.Add(textTo.Text);
				textTo.Clear();
			}
		}
	}
}
